# Malicious Site Wrapper

Malicious Site Wrapper (MSW) is a static HTML document that can be used to wrap
outgoing to links to specific domains. It uses the query parameter `unsafe_url`
to generate appropriate text and links.

## Quickstart

1. Copy the malicious-site-wrapper.html to your favorite static site hosting option.
2. Adjust links to use the query parameter `unsafe_url=https://unsafe-site.org/index.html` (for example).

For example, if the malicious site wrapper is deployed at `https://msw.org`, and
you wanted to warn about links to `https://unsafe-site.org/index.html`, then you
would form links like: `https://msw.org?unsafe_url=https://unsafe-site.org/index.html`.

## Wrapper in Action

The default sample for the wrapper HTML uses Twitter as an example. You can
see it in action using the GitLab Pages deployment embedded into the following
link:

### [https://twitter.com/fossjunkie](https://elmiko.gitlab.io/malicious-site-wrapper?unsafe_url=https://twitter.com/fossjunkie)


```html
<a href="https://elmiko.gitlab.io/malicious-site-wrapper?unsafe_url=https://twitter.com/fossjunke">
https://twitter.com/fossjunkie
</a>
```

## Contributing

This page is relatively simple, but open to all suggestions. The biggest need
currently is for translations of the main body text. See the comments inside
the HTML for instructions on where and how to add this content.

## License

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.
